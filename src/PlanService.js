import axios from 'axios';
class PlanService {
	static async getCategoryTodoReturn() {
		let token = localStorage.getItem('usertoken')
		const res = await axios.get("http://localhost:5000/api/plan/categoryParam/todo", {
			params: {
				token
			}
		})
		return res.data;
	}

	static async getCategoryAppoReturn() {
		let token = localStorage.getItem('usertoken')
		const res = await axios.get("http://localhost:5000/api/plan/categoryParam/appointment", {
			params: {
				token
			}
		})
		return res.data;
	}

	static async getCategoryShopReturn() {
		let token = localStorage.getItem('usertoken')
		const res = await axios.get("http://localhost:5000/api/plan/categoryParam/shopping", {
			params: {
				token
			}
		})
		return res.data;
	}

	static async getCategoryTodoAppoReturn() {
		let token = localStorage.getItem('usertoken')
		const res = await axios.get("http://localhost:5000/api/plan/category/todoAppo", {
			params: {
				token
			}
		})
		return res.data;
	}

	static async getConfigComplete() {
		let token = localStorage.getItem('usertoken')
		const res = await axios.get("http://localhost:5000/api/plan/config", {
			params: {
				token
			}
		})
		return res.data;
	}

	static async getSidebarTags() {
		let token = localStorage.getItem('usertoken')
		const res = await axios.get("http://localhost:5000/api/plan/config", {
			params: {
				token
			}
		})
		return res.data.sidebarTags;
	}

	static async removeSidebarTags(removeTagString) {
		let token = localStorage.getItem('usertoken')
		await axios.post("http://localhost:5000/api/plan/removeSidebarTagsConfig", {
			removeTagString,
			token
		})
	}

	static async addSidebarTags(newTagString) {
		let token = localStorage.getItem('usertoken')
		await axios.post("http://localhost:5000/api/plan/updateSidebarTagsConfig", {
			newTagString,
			token
		})
	}

	static async getDashboardTagsDefault() {
		let token = localStorage.getItem('usertoken')
		const res = await axios.get("http://localhost:5000/api/plan/config", {
			params: {
				token
			}
		})
		return res.data.defaultLists;
	}

	static async setDashboardTagsDefault(defaultLists) {
		let token = localStorage.getItem('usertoken')
		await axios.post("http://localhost:5000/api/plan/updateDefaultList", {
			defaultLists,
			token
		})
	}

	static async getDashboardTagsCustom() {
		let token = localStorage.getItem('usertoken')
		const res = await axios.get("http://localhost:5000/api/plan/config", {
			params: {
				token
			}
		})
		return res.data.customLists;
	}

	static async setDashboardTagsCustom(customLists) {
		let token = localStorage.getItem('usertoken')
		await axios.post("http://localhost:5000/api/plan/updateCustomList", {
			customLists,
			token
		})
	}

	static async checkForUserConfig() {
		let token = localStorage.getItem('usertoken')
		await axios.post("http://localhost:5000/api/plan/checkForUserConfig", {
			token
		})
	}

	static async updateOneCheckedForDelete(payload) {
		let token = localStorage.getItem('usertoken')
		await axios.post("http://localhost:5000/api/plan/updateOneCheckedForDelete", {
			payload,
			token
		})
	}

	static async updateOneEditedItem(payload) {
		let token = localStorage.getItem('usertoken')
		await axios.post("http://localhost:5000/api/plan/updateOneProperties", {
			payload,
			token
		})
	}

	static async postOneNew(payload) {
		let token = localStorage.getItem('usertoken')
		await axios.post("http://localhost:5000/api/plan/oneNew", {
			payload,
			token
		})
	}

	static async getLastCreated() {
		let token = localStorage.getItem('usertoken')
		const res = await axios.get("http://localhost:5000/api/plan/getLastCreated", {
			params: {
				token
			}
		})
		return res.data;
	}

	static async deleteAllChecked(category) {
		var url = "http://localhost:5000/api/plan/deleteAllChecked/" + category;
		await axios.delete(url)
	}

	static async deleteById(payload) {
		var url = "http://localhost:5000/api/plan/del/" + payload;
		await axios.delete(url)
	}

	//
	static splitItemsIntoDateCategoriesHelper(data, tmpDataSplitDate) { //splits data with date into different date categories

		/*var tmpDataSplitDate = {
			today: [],
			tomorrow: [],
			theDayAfterTomorrow: [],
			restOfWeek: [],
			nextWeek: [],
			restOfMonth: [],
			nextMonth: [],
			restOfYear: []
		};
		var tmpDataSplitDate = {};
		Vue.set(tmpDataSplitDate, 'today', [])
		Vue.set(tmpDataSplitDate, 'tomorrow', [])
		Vue.set(tmpDataSplitDate, 'theDayAfterTomorrow', [])
		Vue.set(tmpDataSplitDate, 'restOfWeek', [])
		Vue.set(tmpDataSplitDate, 'nextWeek', [])
		Vue.set(tmpDataSplitDate, 'restOfMonth', [])
		Vue.set(tmpDataSplitDate, 'nextMonth', [])
		Vue.set(tmpDataSplitDate, 'restOfYear', [])
		console.log("hier")
		console.log(tmpDataSplitDate)*/

		const currentTime = new Date();
		currentTime.setHours(0, 0, 0, 0);
		currentTime.setDate(currentTime.getDate());

		//bsp. Sat Jan 04 2020 00:00:00 GMT+0100 (Mitteleuropäsiche Normalzeit)
		var tomorrow = new Date();
		tomorrow.setDate(tomorrow.getDate() + 1);
		tomorrow.setHours(0, 0, 0, 0);

		var theDayAfterTomorrow = new Date();
		theDayAfterTomorrow.setDate(theDayAfterTomorrow.getDate() + 2);
		theDayAfterTomorrow.setHours(0, 0, 0, 0);

		var endOfWeek = new Date();
		var day = currentTime.getDay();
		endOfWeek.setDate(endOfWeek.getDate() + (6 - day) + 1);
		endOfWeek.setHours(0, 0, 0, 0);

		var endOfNextWeek = new Date();
		endOfNextWeek.setDate(endOfWeek.getDate() + 1); //spring zum montag
		endOfNextWeek.setDate(endOfNextWeek.getDate() + 6); //woche nach vorne
		endOfNextWeek.setHours(0, 0, 0, 0);

		var endOfMonth = new Date();
		//while loop um den ersten tag des nächsten monats zu finden
		while (endOfMonth.getMonth() == currentTime.getMonth()) {
			endOfMonth.setDate(endOfMonth.getDate() + 1);
		}
		// -1 ergibt letzten tag des aktuellen Monats
		endOfMonth.setDate(endOfMonth.getDate() - 1);
		endOfMonth.setHours(0, 0, 0, 0);

		var nextMonth = new Date();
		nextMonth.setMonth(nextMonth.getMonth() + 1, 1);
		nextMonth.setHours(0, 0, 0, 0);

		var restOfYearStart = new Date();
		restOfYearStart.setMonth(nextMonth.getMonth() + 1, 1);
		restOfYearStart.setHours(0, 0, 0, 0);

		var restOfYearEnd = new Date();
		restOfYearEnd.setDate(1);
		while (restOfYearEnd.getFullYear() == restOfYearStart.getFullYear()) {
			restOfYearEnd.setMonth(restOfYearEnd.getMonth() + 1);
		}
		restOfYearEnd.setDate(restOfYearEnd.getDate() - 1);
		restOfYearEnd.setHours(0, 0, 0, 0);

		for (var item of data) {
			var tmpSaveStart = new Date(item.start);
			item.start.setHours(0, 0, 0, 0);
			//heute: Tag, Monat und Jahr müssen gleich sein
			if (
				item.start.getMonth() == currentTime.getMonth() &&
				item.start.getDate() == currentTime.getDate() &&
				item.start.getFullYear() == currentTime.getFullYear()
			) {
				item.start = tmpSaveStart;
				if (tmpDataSplitDate.today.includes(item) == false) {
					tmpDataSplitDate.today.push(item);
				}
				//morgen: Tag muss dem morgigen SystemDatum entsprechen (deshalb oben tomorrow +1)
			} else if (tomorrow.getTime() == item.start.getTime()) {
				item.start = tmpSaveStart;
				if (tmpDataSplitDate.tomorrow.includes(item) == false) {
					tmpDataSplitDate.tomorrow.push(item);
				}
				//übermorgen: Tag muss dem SystemDatum von Übermorgen entsprechen (deshalb oben theDayAfterTomorrow +2)
			} else if (theDayAfterTomorrow.getTime() == item.start.getTime()) {
				item.start = tmpSaveStart;
				if (tmpDataSplitDate.theDayAfterTomorrow.includes(item) == false) {
					tmpDataSplitDate.theDayAfterTomorrow.push(item);
				}
				//rest der Woche: Millisekunden Ergebniss muss kleiner als (6 - heutiger Tag + 1) und größer als 0 sein
				//(6-heutigerTag +1) = Maximal 6 weitere Tage bis zum Ende der Woche, davon ziehen wir den heutigen Tag ab, +1 brauchen wir für den Sonntag (hier erster Wochentag). Ergebnis: Abstand von heute bis Sonntag
			} else if (
				endOfWeek.getTime() - item.start.getTime() <=
				1000 * 60 * 60 * 24 * (6 - day + 1) &&
				endOfWeek.getTime() - item.start.getTime() >= 0
			) {
				item.start = tmpSaveStart;
				if (tmpDataSplitDate.restOfWeek.includes(item) == false) {
					tmpDataSplitDate.restOfWeek.push(item);
				}
				//Nächste Woche: endOfNextWeek enspricht Sonntag der nächsten Woche. Millisekunden-Rechnung Ergebniss muss kleiner als 7 Tage(Montag) und größer gleich 0 Tage(Sonntag)
			} else if (
				endOfNextWeek.getTime() - item.start.getTime() <=
				1000 * 60 * 60 * 24 * 6 &&
				endOfNextWeek.getTime() - item.start.getTime() >= 0
			) {
				item.start = tmpSaveStart;
				if (tmpDataSplitDate.nextWeek.includes(item) == false) {
					tmpDataSplitDate.nextWeek.push(item);
				}
			} //Rest des Monats: Millisekunden rechnung ergebnis muss kleiner sein als der Abstand zwischen Ende des Monats und ende nächste Woche (endOfMonth.getDate() - endOfNextWeek.getDate()) und größer gleich null
			else if (
				endOfMonth.getTime() - item.start.getTime() <=
				1000 *
				60 *
				60 *
				24 *
				(endOfMonth.getDate() - endOfNextWeek.getDate()) &&
				endOfMonth.getTime() - item.start.getTime() >= 0
			) {
				item.start = tmpSaveStart;
				if (tmpDataSplitDate.restOfMonth.includes(item) == false) {
					tmpDataSplitDate.restOfMonth.push(item);
				}
			} else if (
				item.start.getMonth() == nextMonth.getMonth() &&
				item.start.getFullYear() == nextMonth.getFullYear()
			) {
				item.start = tmpSaveStart;
				if (tmpDataSplitDate.nextMonth.includes(item) == false) {
					tmpDataSplitDate.nextMonth.push(item);
				}
			} else if (
				restOfYearEnd.getTime() >= item.start.getTime() &&
				restOfYearStart.getTime() <= item.start.getTime()
			) {
				item.start = tmpSaveStart;
				if (tmpDataSplitDate.restOfYear.includes(item) == false) {
					tmpDataSplitDate.restOfYear.push(item);
				}
			}
		}
		return tmpDataSplitDate;
	}
}

export default PlanService;