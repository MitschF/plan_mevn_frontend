<template>
  <div>
    <form id="NewItemAppoForm">
      <div class="headingStyle">
        <label class="headingNewItem">Titel *</label>
      </div>
      <input
        v-bind:class="{ 'input-alert': this.err.title && !this.title}"
        v-model="title"
        class="form-control"
        type="text"
        id="newItemTitle"
        placeholder="Titel eingeben"
      />
      <div class="headingStyle">
        <label class="headingNewItem" for="newItemDesc">Beschreibung</label>
      </div>
      <input
        v-model="description"
        class="form-control"
        type="text"
        id="newItemDesc"
        placeholder="Beschreibung eingeben"
      />
      <div class="headingStyle">
        <label class="headingNewItem" for="startDate">Start *</label>
      </div>
      <div class="box">
        <section>
          <DatePicker
            v-bind:class="{ 'input-alert': this.err.start && !this.start}"
            v-model="start"
            type="datetime"
            placeholder="Startdatum wählen"
            format="YYYY-MM-DD hh:mm"
            :minute-step="5"
            :show-time-panel="showTimePanel"
            :open.sync="openStart"
            @close="handleOpenChange"
          >
            <template v-slot:footer>
              <button
                class="btn btn-dark"
                @click="allDayStartClick()"
                style="margin-right:3px;"
              >ganztägig</button>
              <button
                class="btn btn-dark"
                @click="toggleTimePanel"
              >{{ showTimePanel ? 'Tag auswählen' : 'Uhrzeit auswählen' }}</button>
            </template>
          </DatePicker>
        </section>
      </div>
      <div class="headingStyle">
        <label class="headingNewItem" for="endDate">Ende *</label>
      </div>
      <div class="box">
        <section>
          <DatePicker
            v-bind:class="{ 'input-alert': this.err.end && !this.end}"
            v-model="end"
            type="datetime"
            placeholder="Enddatum wählen"
            format="YYYY-MM-DD hh:mm"
            :minute-step="5"
            :show-time-panel="showTimePanel"
            @close="handleOpenChange"
            :disabled="endDisable"
          >
            <template v-slot:footer>
              <button
                class="btn btn-dark"
                @click="toggleTimePanel"
              >{{ showTimePanel ? 'Tag auswählen' : 'Uhrzeit auswählen' }}</button>
            </template>
          </DatePicker>
        </section>
      </div>
      <div class="headingStyle">
        <label class="headingNewItem" for="reminder">Erinnerung</label>
      </div>
      <div class="box">
        <section>
          <DatePicker
            v-model="reminder"
            type="datetime"
            placeholder="Erinnerung wählen"
            format="YYYY-MM-DD hh:mm"
            :minute-step="5"
            :show-time-panel="showTimePanel"
            @close="handleOpenChange"
          >
            <template v-slot:footer>
              <button
                class="btn btn-dark"
                @click="toggleTimePanel"
              >{{ showTimePanel ? 'Tag auswählen' : 'Uhrzeit auswählen' }}</button>
            </template>
          </DatePicker>
        </section>
      </div>
      <div class="headingStyle">
        <label class="headingNewItem" for="newItemHash">Tags *</label>
      </div>
      <div class="input-group">
        <div class="input-group-prepend">
          <span class="input-group-text" id="inputGroupPrepend2">
            <i class="fas fa-hashtag"></i>
          </span>
        </div>
        <input
          v-bind:class="{ 'input-alert': this.err.hashtag && !this.hashtag}"
          v-model="hashtag"
          class="form-control"
          type="text"
          id="newItemHash"
          placeholder="Stichwort eingeben"
        />
      </div>
      <div v-if="editFlag == true">
        <button type="button" class="btn btn-dark ml-1" @click="checkFormEdit">Speichern</button>
        <button type="button" class="btn btn-dark" @click="deleteById">Löschen</button>
      </div>
      <button v-else type="button" class="btn btn-dark" @click="checkForm">Neues Item</button>
    </form>
  </div>
</template>

<script>
import DatePicker from "vue2-datepicker";
import "vue2-datepicker/locale/de";
import "vue2-datepicker/index.css";

export default {
  data() {
    return {
      category: "appointment",
      hashtag: null,
      title: null,
      description: null,
      start: null,
      end: null,
      priority: null,
      reminder: null,
      recurring: null,
      borderColor: null,
      err: {
        title: false,
        hashtag: false,
        start: false,
        end: false
      },
      showTimePanel: false,
      openStart: false,
      endDisable: false,
      editFlag: false,
      underScoreId: null
    };
  },
  components: {
    DatePicker
  },
  methods: {
    checkForm: function(e) {
      if (this.title && this.hashtag && this.start && this.end) {
        this.hashtagTrim();
        this.newItemToStore();
      }
      this.err.title = false;
      this.err.hashtag = false;
      this.err.start = false;
      this.err.end = false;

      if (!this.title) {
        this.err.title = true;
      }
      if (!this.hashtag) {
        this.err.hashtag = true;
      }
      if (!this.start) {
        this.err.start = true;
      }
      if (!this.end) {
        this.err.end = true;
      }
      e.preventDefault();
    },
    hashtagTrim: function() {
      if (this.hashtag) {
        let hashtagTrim = this.hashtag.toLowerCase();
        this.hashtag = hashtagTrim.replace(/\s+/g, "-");
      }
    },
    hashtagToColor(hashtag) {
      var hash = 5381;
      for (var i = 0; i < hashtag.length; i++) {
        hash = hash * 33 + hashtag.charCodeAt(i);
      }
      var hue = hash % 360;
      return "hsl(" + Math.round(hue) + ", 35%, 50%)";
    },
    newItemToStore: async function() {
      let colorPicker = this.hashtagToColor(this.hashtag);

      let payload = {
        category: this.category,
        hashtag: this.hashtag,
        title: this.title,
        description: this.description,
        start: this.start,
        end: this.end,
        reminder: this.reminder,
        backgroundColor: colorPicker
      };

      this.$store.dispatch("postNewItem", payload).then(() => {
        this.emptyEntryFiels();
      });
      this.$store.commit("deleteTempAppo");
    },
    prefillEntryFields(item) {
      this.emptyEntryFiels();

      this.start = item.start;
      this.end = item.end;
    },
    toggleTimePanel() {
      this.showTimePanel = !this.showTimePanel;
    },
    handleOpenChange() {
      this.showTimePanel = false;
    },
    allDayStartClick() {
      if (this.start != null) {
        this.start.setHours(0, 0, 0, 0);
        this.end = new Date(this.start);
        this.end.setDate(this.start.getDate() + 1);
        this.openStart = false;
        this.endDisable = true;
      } else {
        this.err.push("Startdatum auswählen");
      }
    },
    loadToEditAppoItem(item) {
      this.editFlag = true;
      this.underScoreId = item._id;
      this.title = item.title;
      this.description = item.description;
      this.start = item.start;
      this.end = item.end;
      this.reminder = item.reminder;
      this.hashtag = item.hashtag;
      this.borderColor = item.borderColor;
    },
    checkFormEdit(e) {
      if (this.title && this.hashtag && this.start && this.end) {
        this.hashtagTrim();
        let colorPicker = this.hashtagToColor(this.hashtag);

        var payload = {
          _id: this.underScoreId,
          category: this.category,
          title: this.title,
          hashtag: this.hashtag,
          description: this.description,
          start: this.start,
          end: this.end,
          reminder: this.reminder,
          recurring: this.recurring,
          backgroundColor: colorPicker,
          borderColor: this.borderColor
        };

        this.$store.dispatch("updateEditedItemAction", payload).then(() => {
          this.emptyEntryFiels();
        });
        this.editFlag = false;
      }
      this.err = [];
      if (!this.title) {
        this.err.title = true;
      }
      if (!this.hashtag) {
        this.err.hashtag = true;
      }
      if (!this.start) {
        this.err.start = true;
      }
      if (!this.end) {
        this.err.end = true;
      }
      e.preventDefault();
    },
    deleteById() {
      this.$store.dispatch("deleteItem", this.underScoreId).then(() => {
        this.emptyEntryFiels();
      });
    },
    emptyEntryFiels() {
      this.title = null;
      this.hashtag = null;
      this.description = null;
      this.start = null;
      this.end = null;
      this.reminder = null;
      this.endDisable = false;
    }
  },
  mounted() {
    this.$root.$on("calendarItemClicked", item => {
      this.prefillEntryFields(item);
    });
    this.$root.$on("EventEditAppoItem", item => {
      this.loadToEditAppoItem(item);
    });
  }
};
</script>

<style scoped>
.mx-datepicker {
  width: 100%;
}
.mx-datepicker >>> .mx-input {
  color: initial;
}

.input-alert {
  outline: red solid 1px;
}
</style>
