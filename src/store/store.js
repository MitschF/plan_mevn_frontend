import Vue from 'vue';
import Vuex from 'vuex';
import PlanService from '../PlanService';
Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		userdata: {
			items: {
				todoAppo: [],
				shop: {
					trockensortiment: [],
					drogeriewaren: [],
					milchprodukte: [],
					tiefkühlkost: [],
					fleischWurst: [],
					obstGemüse: [],
					brot: [],
					getränke: [],
					süßwarenSnacks: [],
					sonstiges: []
				}
			}
		}
	},
	getters: {
		getItemsByCategory(state) {
			return (category) => {
				if (category == 'shopping') {
					return state.userdata.items.shop;
				} else {
					var data = [];
					for (var itemTodoAppo of state.userdata.items.todoAppo) {
						if (itemTodoAppo.category == category) {
							Vue.set(itemTodoAppo, 'id', itemTodoAppo._id); // "id" Feld wird nur für den Kalender benötigt, unser _id Feld reicht nicht aus
							data.push(itemTodoAppo);
						}
					}
				}
				return data;
			};
		},
		getItemsByHashtag(state) {
			return (hashtag) => {
				var data = [];
				for (var itemTodoAppo of state.userdata.items.todoAppo) {
					if (itemTodoAppo.hashtag != null) {
						if (itemTodoAppo.hashtag == hashtag) {
							data.push(itemTodoAppo);
						}
					}
				}
				data.sort(function (a, b) {
					return new Date(a.start) - new Date(b.start);
				})
				return data;
			};
		},
		getItemsByHashtagWithoutDate(state) {
			return (hashtag) => {
				var data = [];
				for (var itemTodoAppo of state.userdata.items.todoAppo) {
					if (itemTodoAppo.hashtag != null) {
						if (itemTodoAppo.start == null) {
							if (itemTodoAppo.hashtag == hashtag) {
								data.push(itemTodoAppo);
							}
						}
					}
				}
				return data;
			};
		},
		getItemsByHashtagWithDateWithDateCategories(state) {
			return (hashtag, objectToReturn) => {
				var storeWithDate = [];
				for (var itemTodoAppo of state.userdata.items.todoAppo) {
					if (itemTodoAppo.hashtag != null) {
						if (itemTodoAppo.hashtag == hashtag) {
							if (itemTodoAppo.start != null) {
								storeWithDate.push(itemTodoAppo);
							}
						}
					}
				}
				storeWithDate.sort(function (a, b) {
					return new Date(a.start) - new Date(b.start);
				});

				PlanService.splitItemsIntoDateCategoriesHelper(storeWithDate, objectToReturn);

				return objectToReturn;
			};
		},
		getTodoAppoWithoutDate(state) {
			var storeWithoutDate = [];
			for (var item of state.userdata.items.todoAppo) {
				if (item.start == null) {
					storeWithoutDate.push(item);
				}
			}
			return storeWithoutDate;
		},
		getTodoAppoWithDateSortedByDate(state) {
			var storeWithDate = [];
			for (var item of state.userdata.items.todoAppo) {
				if (item.start != null) {
					storeWithDate.push(item);
				}
			}
			storeWithDate.sort(function (a, b) {
				return new Date(a.start) - new Date(b.start);
			});
			return storeWithDate;
		},
		getTodoAppoWithDateWithDateCategories(state) {
			return (objectToReturn) => {
				var storeWithDate = [];
				for (var item of state.userdata.items.todoAppo) {
					if (item.start != null) {
						storeWithDate.push(item);
					}
				}
				storeWithDate.sort(function (a, b) {
					return new Date(a.start) - new Date(b.start);
				});


				PlanService.splitItemsIntoDateCategoriesHelper(storeWithDate, objectToReturn);

				return objectToReturn;
			}
		},
		getTodoAppo7DaysWithDateWithDateCategories(state) {
			return (objectToReturn) => {
				var storeWithDate = [];
				var currentDate = new Date();
				currentDate.setHours(0, 0, 0, 0)
				var sevenDaysAhead = new Date();
				sevenDaysAhead.setHours(23, 59, 59, 999)
				sevenDaysAhead.setDate(sevenDaysAhead.getDate() + 7);
				for (var item of state.userdata.items.todoAppo) {
					if (item.start != null && item.start < sevenDaysAhead && item.start > currentDate) {
						storeWithDate.push(item);
					}
				}
				storeWithDate.sort(function (a, b) {
					return new Date(a.start) - new Date(b.start);
				})
				return PlanService.splitItemsIntoDateCategoriesHelper(storeWithDate, objectToReturn);
			}

		},
		getOurItemByCalendarItemId(state) {
			return (itemId) => {
				for (var itemLoop of state.userdata.items.todoAppo) {
					if (itemLoop._id == itemId) {
						return itemLoop;
					}
				}
			};
		}
	},
	mutations: {
		setItemsTodoAppo(state, payload) {
			for (var item of payload) {
				if (item.start != null) {
					item.start = new Date(item.start);
				}
				if (item.end != null) {
					item.end = new Date(item.end);
				}
				if (item.reminder != null) {
					item.reminder = new Date(item.reminder);
				}
				state.userdata.items.todoAppo.push(item);
			}
		},
		setItemsShop(state, payload) {
			for (var item of payload) {
				state.userdata.items.shop[item.shopCat].push(item);
			}
		},
		pushItem(state, payload) {
			if (payload.category == 'todo') {
				state.userdata.items.todoAppo.push(payload);
			} else if (payload.category == 'shopping') {
				state.userdata.items.shop[payload.shopCat].push(payload);
			} else if (payload.category == 'appointment') {
				if (Array.isArray(payload) == false) {
					state.userdata.items.todoAppo.push(payload);
				} else {
					for (var item of payload) {
						state.userdata.items.todoAppo.push(item);
					}
				}
			}
		},
		deleteAllChecked(state, category) {
			if (category == "todo" || category == "both") {
				for (i = state.userdata.items.todoAppo.length - 1; i >= 0; i--) {
					if (state.userdata.items.todoAppo[i].checkedForDelete == true) {
						state.userdata.items.todoAppo.splice(i, 1);
					}
				}
			}
			if (category == "shopping" || category == "both") {
				for (var shopCat in state.userdata.items.shop) {
					for (var i = state.userdata.items.shop[shopCat].length - 1; i >= 0; i--) {
						if (state.userdata.items.shop[shopCat][i].checkedForDelete == true) {
							state.userdata.items.shop[shopCat].splice(i, 1);
						}
					}
				}
			}
		},
		changeCheckedForDeleteStore(state, item) {
			if (item.category == 'shopping') {
				for (var shopcat in state.userdata.items.shop)
					for (var itemLoop of state.userdata.items.shop[shopcat]) {
						if (itemLoop._id != null && itemLoop._id == item._id) {
							itemLoop.checkedForDelete = !item.checkedForDelete;
							break;
						}
					}
			} else if (item.category != 'shopping') {
				for (itemLoop of state.userdata.items.todoAppo) {
					if (itemLoop._id != null && itemLoop._id == item._id) {
						itemLoop.checkedForDelete = !item.checkedForDelete;
						break;
					}
				}
			}
		},
		updateEditedItem(state, item) {
			if (item.category == 'shopping') {
				for (var shopcat in state.userdata.items.shop)
					for (var itemLoop of state.userdata.items.shop[shopcat]) {
						if (itemLoop._id != null && itemLoop._id == item._id) {
							itemLoop.title = item.title;
							itemLoop.shopCat = item.shopCat;
							itemLoop.checkedForDelete = item.checkedForDelete;
							break;
						}
					}
			} else if (item.category == 'todo') {
				for (itemLoop of state.userdata.items.todoAppo) {
					if (itemLoop._id != null && itemLoop._id == item._id) {
						itemLoop.title = item.title;
						itemLoop.description = item.description;
						itemLoop.priority = item.priority;
						itemLoop.start = item.start;
						itemLoop.end = item.end;
						itemLoop.reminder = item.reminder;
						itemLoop.hashtag = item.hashtag;
						itemLoop.recurring = item.recurring;
						itemLoop.checkedForDelete = item.checkedForDelete;
						break;
					}
				}
			} else if (item.category == 'appointment') {
				for (itemLoop of state.userdata.items.todoAppo) {
					if (itemLoop._id != null && itemLoop._id == item._id) {
						itemLoop.title = item.title;
						itemLoop.description = item.description;
						itemLoop.start = item.start;
						itemLoop.end = item.end;
						itemLoop.reminder = item.reminder;
						itemLoop.hashtag = item.hashtag;
						itemLoop.recurring = item.recurring;
						itemLoop.backgroundColor = item.backgroundColor;
						itemLoop.borderColor = item.borderColor;
						break;
					}
				}
			}
		},
		deleteTempAppo(state) {
			for (var item of state.userdata.items.todoAppo) {
				item.borderColor = 'white';
			}
			var index = state.userdata.items.todoAppo.length - 1;
			if (
				state.userdata.items.todoAppo[index].tempFinder != null &&
				state.userdata.items.todoAppo[index].tempFinder == true
			) {
				state.userdata.items.todoAppo.splice(index, 1);
			}
		},
		calenderEventClickedStyle(state, payload) {
			for (var item of state.userdata.items.todoAppo) {
				item.borderColor = 'white';
			}
			if (payload == '') {
				for (item of state.userdata.items.todoAppo) {
					if (item.tempFinder == true) {
						item.borderColor = 'black';
					}
				}
			} else {
				for (item of state.userdata.items.todoAppo) {
					if (item.id == payload) {
						item.borderColor = 'black';
					}
				}
			}
		},
		mutateDefaultTagAction(state, item) {
			state.userconfig.defaultLists.item = !item;
		},
		deleteItem(state, id) {
			for (var item of state.userdata.items.todoAppo) {
				if (item._id == id) {
					state.userdata.items.todoAppo.splice(state.userdata.items.todoAppo.indexOf(item), 1);
					break;
				}
			}

			for (var shopCat in state.userdata.items.shop) {
				for (item of state.userdata.items.shop[shopCat]) {
					if (item._id == id) {
						state.userdata.items.shop[shopCat].splice(state.userdata.items.shop[shopCat].indexOf(item), 1);
						break;
					}
				}
			}
		}
	},
	actions: {
		importDbTodoAppo(context, payload) {
			context.commit("setItemsTodoAppo", payload);
		},
		importDbShop(context, payload) {
			context.commit("setItemsShop", payload);
		},
		async deleteItem(context, id) {
			context.commit('deleteItem', id);
			await PlanService.deleteById(id);
		},
		async checkedForDeleteAction(context, item) {
			await PlanService.updateOneCheckedForDelete(item);

			context.commit('changeCheckedForDeleteStore', item);
		},
		async updateEditedItemAction(context, item) {
			context.commit('updateEditedItem', item);

			await PlanService.updateOneEditedItem(item);
		},
		async postNewItem(context, item) {
			await PlanService.postOneNew(item); //neues item in die db, hat ab jetzt eine _id

			var newItemWithId = await PlanService.getLastCreated(); //letztes item der db (sollte immer das neuste erstellte sein) wird runtergeladen

			if (newItemWithId.start != null) {
				newItemWithId.start = new Date(newItemWithId.start); //time conversion: String --> Date
			}
			if (newItemWithId.end != null) {
				newItemWithId.end = new Date(newItemWithId.end);
			}
			if (newItemWithId.reminder != null) {
				newItemWithId.reminder = new Date(newItemWithId.reminder);
			}

			context.commit("pushItem", newItemWithId); //letztes item der db wird in den store gepushed --> neues Item im Store
		},
		async deleteAllCheckedAction(context, category) {
			context.commit("deleteAllChecked", category);

			await PlanService.deleteAllChecked(category);
		}
	}
});