import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
	{
		path: '/',
		name: 'dashboard',
		component: () => import('../views/Dashboard.vue'),
		beforeEnter: (to, from, next) => {
			if (localStorage.getItem('usertoken')) next()
			else next('/login');
		}

	},
	{
		path: '/calendar',
		name: 'calendar',
		component: () => import('../views/Calendar.vue'),
		beforeEnter: (to, from, next) => {
			if (localStorage.getItem('usertoken')) next()
			else next('/login');
		}
	},
	{
		path: '/todos',
		name: 'todos',
		component: () => import('../views/ToDos.vue'),
		beforeEnter: (to, from, next) => {
			if (localStorage.getItem('usertoken')) next()
			else next('/login');
		}
	},
	{
		path: '/groceries',
		name: 'groceries',
		component: () => import('../views/Groceries.vue'),
		beforeEnter: (to, from, next) => {
			if (localStorage.getItem('usertoken')) next()
			else next('/login');
		}
	},
	{
		path: '/all',
		name: 'all',
		component: () => import('../views/All.vue'),
		beforeEnter: (to, from, next) => {
			if (localStorage.getItem('usertoken')) next()
			else next('/login');
		}
	},
	{
		path: '/list/:tag',
		name: 'list',
		component: () => import('../views/List.vue'),
		beforeEnter: (to, from, next) => {
			if (localStorage.getItem('usertoken')) next()
			else next('/login');
		}
	},
	// USER-LOGIN 
	{
		path: '/login',
		name: 'login',
		component: () => import('../views/userProfile/Login.vue'),
		beforeEnter: (to, from, next) => {
			if (localStorage.getItem('usertoken')) next('/')
			else next();
		}
	},
	{
		path: '/register',
		name: 'register',
		component: () => import('../views/userProfile/Register.vue'),
		beforeEnter: (to, from, next) => {
			if (localStorage.getItem('usertoken')) next('/profile')
			else next();
			}
	},
	{
		path: '/profile',
		name: 'profile',
		component: () => import('../views/userProfile/Profile.vue'),
		beforeEnter: (to, from, next) => {
			if (localStorage.getItem('usertoken')) next()
			else next('/login')
		}
	},
];

const router = new VueRouter({
	routes
});

export default router;
